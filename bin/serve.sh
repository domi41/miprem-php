#!/usr/bin/env bash

# Run the PHP built-in web server at port 8000.

set -e

php -S localhost:8000 demo.php
