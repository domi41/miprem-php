<?php
require_once ("./vendor/autoload.php");


$poll = [
    "question" => ["label" => "A test poll"],
    "tally" => [
        [21, 14, 32, 31, 24, 42, 9],
        [12, 16, 39, 9, 31, 24, 42],
        [42, 9, 21, 14, 32, 31, 24],
        [31, 24, 42, 12, 16, 39, 9]
    ]
];

$config = ["width" => 600];
$style = "";

$miprem = new Miprem\Renderer(Miprem\Template::MERIT_PROFILE, $config, $style);
$svg = $miprem->render($poll);

echo $svg;
