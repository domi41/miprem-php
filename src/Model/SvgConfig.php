<?php

namespace Miprem\Model;

class SvgConfig extends AbstractModel
{
    const DEFAULT_WIDTH = 600;
    const DEFAULT_HEIGHT = 250;
    const DEFAULT_SIDEBAR_WIDTH = 35;
    const DEFAULT_HEADER_HEIGHT = 40;
    const DEFAULT_PADDING = 10;
    const DEFAULT_HORIZONTAL_GAP = 5;
    const DEFAULT_VERTICAL_GAP = 5;
    const DEFAULT_TOOLTIP = 'title';
    const DEFAULT_GRADES_COLOR_PALETTE = ['#e63e11', '#ee8100', '#fecb02', '#85bb2f', '#038141'];
    const DEFAULT_REVERSE_GRADES_COLORS = false;
    const DEFAULT_REVERSE_GRADES = false;
    const DEFAULT_CUSTOM_CSS = '/* #background {} */
/* #question {} */
/* #median_line {} */
/* #error {} */
/* .h-gap {} */
/* .proposal-ref {} */
/* .proposal-ref circle {} */
/* .proposal-ref text {} */
/* .svg-tooltip {} */';

    private int $width;
    private int $height;
    private int $sidebarWidth;
    private int $headerHeight;
    private int $padding;
    private int $horizontalGap;
    private int $verticalGap;
    private string $tooltip;
    private array $gradesColorPalette;
    private bool $reverseGradesColors;
    private bool $reverseGrades;
    private string $customCss;


    public function __construct(
        int $width = self::DEFAULT_WIDTH,
        int $height = self::DEFAULT_HEIGHT,
        int $sidebarWidth = self::DEFAULT_SIDEBAR_WIDTH,
        int $headerHeight = self::DEFAULT_HEADER_HEIGHT,
        int $padding = self::DEFAULT_PADDING,
        int $horizontalGap = self::DEFAULT_HORIZONTAL_GAP,
        int $verticalGap = self::DEFAULT_VERTICAL_GAP,
        string $tooltip = self::DEFAULT_TOOLTIP,
        array $gradesColorPalette = self::DEFAULT_GRADES_COLOR_PALETTE,
        bool $reverseGradesColors = self::DEFAULT_REVERSE_GRADES_COLORS,
        bool $reverseGrades = self::DEFAULT_REVERSE_GRADES,
        string $customCss = self::DEFAULT_CUSTOM_CSS)
    {
        $this->width = $width;
        $this->height = $height;
        $this->sidebarWidth = $sidebarWidth;
        $this->headerHeight = $headerHeight;
        $this->padding = $padding;
        $this->horizontalGap = $horizontalGap;
        $this->verticalGap = $verticalGap;
        $this->tooltip = $tooltip;
        $this->gradesColorPalette = $gradesColorPalette;
        $this->reverseGradesColors = $reverseGradesColors;
        $this->reverseGrades = $reverseGrades;
        $this->customCss = $customCss;
    }

    // **********************
    // *** Static methods ***
    // **********************

    public static function sample() : self
    {
        return new self();
    }

    public static function fromArray(array $svgConfig) : self
    {
        return new self(
            $svgConfig['width'] ?? self::DEFAULT_WIDTH,
            $svgConfig['height'] ?? self::DEFAULT_HEIGHT,
            $svgConfig['sidebar_width'] ?? self::DEFAULT_SIDEBAR_WIDTH,
            $svgConfig['header_height'] ?? self::DEFAULT_HEADER_HEIGHT,
            $svgConfig['padding'] ?? self::DEFAULT_PADDING,
            $svgConfig['horizontal_gap'] ?? self::DEFAULT_HORIZONTAL_GAP,
            $svgConfig['vertical_gap'] ?? self::DEFAULT_VERTICAL_GAP,
            $svgConfig['tooltip'] ?? self::DEFAULT_TOOLTIP,
            $svgConfig['grades_color_palette'] ?? self::DEFAULT_GRADES_COLOR_PALETTE,
            $svgConfig['reverse_grades_colors'] ?? self::DEFAULT_REVERSE_GRADES_COLORS,
            $svgConfig['reverse_grades'] ?? self::DEFAULT_REVERSE_GRADES,
            $svgConfig['custom_css'] ?? self::DEFAULT_CUSTOM_CSS);
    }

    public static function fromYaml(string $svgConfigYaml) : self
    {
        return self::fromArray(yaml_parse($svgConfigYaml) ?? []);
    }

    public static function fromQueryString(string $svgConfigQueryString) : self
    {
        parse_str($svgConfigQueryString ?? '', $query);
        if (array_key_exists('grades_color_palette', $query)) {
            $query['grades_color_palette'] = explode('-', $query['grades_color_palette']);
        }
        if (array_key_exists('reverse_grades_colors', $query)) {
            $query['reverse_grades_colors'] = $query['reverse_grades_colors'] != 'n';
        }
        if (array_key_exists('reverse_grades', $query)) {
            $query['reverse_grades'] = $query['reverse_grades'] != 'n';
        }
        return self::fromArray($query);
    }

    public static function minifyCss($css) : string
    {
        $css = preg_replace('/\/\*((?!\*\/).)*\*\//', '', $css);
        $css = preg_replace('/\s{2,}/', ' ', $css);
        $css = preg_replace('/\s*([:;{}])\s*/', '$1', $css);
        $css = preg_replace('/;}/', '}', $css);
        return $css;
    }

    public static function prettifyCss($css)
    {
        $blocks = preg_split('/\/\*.*\*\//', $css);
        preg_match_all('/\/\*.*\*\//', $css, $comments);

        foreach ($blocks as $key => $block) {
            $blocks[$key] = preg_replace('/\{/', " {\n\t", $blocks[$key]);
            $blocks[$key] = preg_replace('/;([^}])/', ";\n\t$1", $blocks[$key]);
            $blocks[$key] = preg_replace('/;\}/', ";\n}\n\n", $blocks[$key]);
            $blocks[$key] = preg_replace('/([^\n])\}/', "$1;\n}\n\n", $blocks[$key]);
            $blocks[$key] = preg_replace('/,/', ",\n", $blocks[$key]);
            $blocks[$key] = preg_replace('/\t(.*):(.*);/', "\t$1: $2;", $blocks[$key]);
            $blocks[$key] .= $comments[0][$key] ?? '';
        }

        return trim(join('', $blocks));
    }

    // **********************
    // *** Public methods ***
    // **********************

    public function toArray() : array
    {
        return [
            'width' => $this->width,
            'height' => $this->height,
            'sidebar_width' => $this->sidebarWidth,
            'header_height' => $this->headerHeight,
            'padding' => $this->padding,
            'horizontal_gap' => $this->horizontalGap,
            'vertical_gap' => $this->verticalGap,
            'tooltip' => $this->tooltip,
            'grades_color_palette' => $this->gradesColorPalette,
            'reverse_grades_colors' => $this->reverseGradesColors,
            'reverse_grades' => $this->reverseGrades,
            'custom_css' => $this->customCss
        ];
    }

    public function toYaml() : string
    {
        return substr(substr(yaml_emit($this->toArray()), 3), 0, -5);
    }

    public function toQueryString() : string
    {
        $svg_config_array = [];

        if ($this->width != self::DEFAULT_WIDTH) {
            $svg_config_array['width'] = $this->width;
        }

        if ($this->height != self::DEFAULT_HEIGHT) {
            $svg_config_array['height'] = $this->height;
        }

        if ($this->sidebarWidth != self::DEFAULT_SIDEBAR_WIDTH) {
            $svg_config_array['sidebar_width'] = $this->sidebarWidth;
        }

        if ($this->headerHeight != self::DEFAULT_HEADER_HEIGHT) {
            $svg_config_array['header_height'] = $this->headerHeight;
        }

        if ($this->padding != self::DEFAULT_PADDING) {
            $svg_config_array['padding'] = $this->padding;
        }

        if ($this->horizontalGap != self::DEFAULT_HORIZONTAL_GAP) {
            $svg_config_array['horizontal_gap'] = $this->horizontalGap;
        }

        if ($this->verticalGap != self::DEFAULT_VERTICAL_GAP) {
            $svg_config_array['vertical_gap'] = $this->verticalGap;
        }

        if ($this->tooltip != self::DEFAULT_TOOLTIP) {
            $svg_config_array['tooltip'] = $this->tooltip;
        }

        if ($this->gradesColorPalette != self::DEFAULT_GRADES_COLOR_PALETTE) {
            $svg_config_array['grades_color_palette'] = join('-', $this->gradesColorPalette ?? []);
        }

        if ($this->reverseGradesColors != self::DEFAULT_REVERSE_GRADES_COLORS) {
            $svg_config_array['reverse_grades_colors'] = $this->reverseGradesColors ? 'y' : 'n';
        }

        if ($this->reverseGrades != self::DEFAULT_REVERSE_GRADES) {
            $svg_config_array['reverse_grades'] = $this->reverseGrades ? 'y' : 'n';
        }

        if ($this->customCss != self::DEFAULT_CUSTOM_CSS) {
            $svg_config_array['custom_css'] = trim(self::minifyCSS($this->customCss));
        }

        return http_build_query($svg_config_array);
    }

    public function merge(AbstractModel $that) : AbstractModel
    {
        return self::fromArray(array_merge($this->toArray(), $that->toArray()));
    }

    // ***************
    // *** Setters ***
    // ***************

    /**
     * @param int $width
     */
    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @param int $sidebarWidth
     */
    public function setSidebarWidth(int $sidebarWidth): self
    {
        $this->sidebarWidth = $sidebarWidth;

        return $this;
    }

    /**
     * @param int $headerHeight
     */
    public function setHeaderHeight(int $headerHeight): self
    {
        $this->headerHeight = $headerHeight;

        return $this;
    }

    /**
     * @param int $padding
     */
    public function setPadding(int $padding): self
    {
        $this->padding = $padding;

        return $this;
    }

    /**
     * @param int $horizontalGap
     */
    public function setHorizontalGap(int $horizontalGap): self
    {
        $this->horizontalGap = $horizontalGap;

        return $this;
    }

    /**
     * @param int $verticalGap
     */
    public function setVerticalGap(int $verticalGap): self
    {
        $this->verticalGap = $verticalGap;

        return $this;
    }

    /**
     * @param string $tooltip
     */
    public function setTooltip(string $tooltip): self
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    /**
     * @param array $gradesColorPalette
     */
    public function setGradesColorPalette(array $gradesColorPalette): self
    {
        $this->gradesColorPalette = $gradesColorPalette;

        return $this;
    }

    /**
     * @param bool $reverseGradesColors
     */
    public function setReverseGradesColors(bool $reverseGradesColors): self
    {
        $this->reverseGradesColors = $reverseGradesColors;

        return $this;
    }

    /**
     * @param bool $reverseGrades
     */
    public function setReverseGrades(bool $reverseGrades): self
    {
        $this->reverseGrades = $reverseGrades;

        return $this;
    }

    /**
     * @param string $custom_css
     */
    public function setCustomCss(string $customCss): self
    {
        $this->customCss = $customCss;

        return $this;
    }

    // ***************
    // *** Getters ***
    // ***************

    public function getWidth() : int {
        return $this->width;
    }

    public function getHeight() : int {
        return $this->height;
    }

    public function getSidebarWidth() : int {
        return $this->sidebarWidth;
    }

    public function getHeaderHeight() : int {
        return $this->headerHeight;
    }

    public function getPadding() : int {
        return $this->padding;
    }

    public function getHorizontalGap() : int {
        return $this->horizontalGap;
    }

    public function getVerticalGap() : int {
        return $this->verticalGap;
    }

    public function getTooltip() : string {
        return $this->tooltip;
    }

    public function getGradesColorPalette() : array {
        return $this->gradesColorPalette;
    }

    public function getReverseGradesColors() : bool {
        return $this->reverseGradesColors;
    }

    public function getReverseGrades() : bool {
        return $this->reverseGrades;
    }

    public function getCustomCss() : string {
        return $this->customCss;
    }

}
