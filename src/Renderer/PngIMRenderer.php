<?php

namespace Miprem\Renderer;


class PngIMRenderer extends SvgRenderer
{

    public function render(\Miprem\Model\Poll $poll, array $opt = []): string
    {
        $svg = parent::render($poll);
        $im = new \Imagick();
        $im->readImageBlob($svg);
        $im->setImageFormat('png24');
        return $im;
    }

    public function getFileExtension() : string
    {
        return '.png';
    }

}
