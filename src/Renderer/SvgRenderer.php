<?php

namespace Miprem\Renderer;

use Twig\Twig;

class SvgRenderer extends AbstractRenderer
{

    protected \Miprem\Model\SvgConfig $svgConfig;
    private \Twig\TemplateWrapper $template;

    public function __construct(\Miprem\Model\SvgConfig $svgConfig)
    {
        $this->svgConfig = $svgConfig;

        $this->initTwig();
        $this->twig->addFilter(new \Twig\TwigFilter('grade_color', [$this, 'gradeColor']));
        $this->template = $this->twig->load('merit_profile.svg');
    }

    public function render(\Miprem\Model\Poll $poll, array $opt = []) : string
    {
        return $this->template->render([
            'poll' => $poll,
            'config' => $this->svgConfig,
            'error' => $opt['error'] ?? ''
        ]);
    }

    public function getIdentifier(\Miprem\Model\Poll $poll, array $opt = []) : string
    {
        return md5(json_encode([
            'config' => $this->svgConfig->toArray(),
            'poll' => $poll->toArray(),
            'opt' => $opt
        ]));
    }

    public function getFileExtension() : string
    {
        return '.svg';
    }

    public function gradeColor(int $gradeId, int $amountOfGrades, array $gradesColorPalette) : string
    {
        $color_id = $gradeId / ($amountOfGrades - 1) * (count($gradesColorPalette) - 1);

        $dec_part = $color_id - floor($color_id);
        $start_col = $gradesColorPalette[floor($color_id)] ?? end($grades_color_palette);
        $end_col = $gradesColorPalette[ceil($color_id)] ?? end($grades_color_palette);

        $rgb_linear = function(string $color_start, string $color_end, float $alpha) {
            $rgb_start = sscanf($color_start, "#%02x%02x%02x");
            $rgb_end = sscanf($color_end, "#%02x%02x%02x");
            $rgb = array_map(function(int $col1, int $col2) use ($alpha) {
                return round($col1 * (1 - $alpha) + $alpha * $col2);
            }, $rgb_start, $rgb_end);
            return sprintf("#%02x%02x%02x", ...$rgb);
        };

        return $dec_part == 0 ? $start_col : $rgb_linear($start_col, $end_col, $dec_part);
    }

}
