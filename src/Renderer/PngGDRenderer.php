<?php

namespace Miprem\Renderer;

use SVG\SVG;


class PngGDRenderer extends SvgRenderer
{

    public function render(\Miprem\Model\Poll $poll, array $opt = []): string
    {
        $svg = SVG::fromString(parent::render($poll, $opt));
        $doc = $svg->getDocument();
        $doc->addFont(__DIR__ . '/DejaVuSans.ttf', null, 'sans-serif');
        $raster = $svg->toRasterImage($doc->getWidth(), $doc->getHeight());

        ob_start();
        imagepng($raster);
        return ob_get_clean();
    }

    public function getFileExtension() : string
    {
        return '.png';
    }

}
